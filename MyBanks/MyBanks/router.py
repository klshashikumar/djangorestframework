from Bank.viewset import BankViewSet, BranchViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('bank',BankViewSet)
router.register('branch',BranchViewSet)


for url in router.urls:
    print(url,'\n')

