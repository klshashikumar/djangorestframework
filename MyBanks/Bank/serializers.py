from rest_framework import serializers
from .models import Branches, Banks


class BankSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Banks
        fields = '__all__'

class BranchSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Branches 
        fields = '__all__'