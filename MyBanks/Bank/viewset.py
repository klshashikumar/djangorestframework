from Bank.models import Branches,Banks
from .serializers import BranchSerializer, BankSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response


class BranchViewSet(viewsets.ModelViewSet):
    queryset = Branches.objects.all()
    serializer_class = BranchSerializer

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('branch').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

class BankViewSet(viewsets.ModelViewSet):
    queryset = Banks.objects.all()
    serializer_class = BankSerializer

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('name').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)
