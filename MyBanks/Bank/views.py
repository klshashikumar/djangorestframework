from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse
from .models import Banks, Branches
from rest_framework import viewsets
from django.views.generic import ListView, DetailView
# Create your views here.


class BankListView(ListView):
    model = Banks
    template_name = 'list.html'

class BankDetailView(DetailView):
    model = Banks
    template_name = 'details.html'


class BranchListView(ListView):
    model = Branches
    template_name = 'list.html'

class BranchDetailView(DetailView):
    model = Branches
    template_name = 'details.html'


        